const requestURL = "https://ajax.test-danit.com/api/swapi/films";
const div = document.createElement("div");
const ul = document.createElement("ul");
const li = document.createElement("li");
document.body.append(div);
div.append(ul);
function sendRequest(url) {
  return fetch(url).then(response => {
    return response.json();
  });
}
sendRequest(requestURL).then(data => {
  console.log(data);
  data.forEach(element => {
    console.log(element);
    element.characters.forEach(response => {
      fetch(response)
        .then(iterator => {
          return iterator.json();
        })
        .then(res => {
          const span = document.getElementById(`span${element.id}`);
          span.append(`${res.name}`);
          console.log(res);
        });
      console.log(element.episodeId, element.name, element.openingCrawl);
    });

    return ul.insertAdjacentHTML(
      "beforeend",
      `<li> Епізод  №${element.id} <br> Назва ${element.name} <br> Hero: <span id="span${element.id}"> </span> <br> Опис: ${element.openingCrawl}  </li>`
    );
  });
  console.log(data);
});
